# Install raspi-config

Script to install raspi-config on Raspberry Pi devices with an Linux OS that's missing it.
Must be run as root or with sudo.

sudo sh install.sh


## Note

This repo is a clone of this repo: [https://github.com/snubbegbg/install_raspi-config](https://github.com/snubbegbg/install_raspi-config).  

I don't know much about shell scripts, and I'll be tweaking this, so that's my disclaimer ;-)